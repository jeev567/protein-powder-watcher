package com.app.proteinpowderwatcher;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.proteinpowderwatcher.job.ProtienPriceSynchJob;
import com.app.proteinpowderwatcher.model.Alert;
import com.app.proteinpowderwatcher.model.Report;
import com.app.proteinpowderwatcher.repository.AlertRepository;
import com.app.proteinpowderwatcher.repository.ReportRepository;
import com.app.proteinpowderwatcher.service.WatchersServ;
import com.app.proteinpowderwatcher.util.JobStatus;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProteinPowderWatcherApplicationTests {

	@Autowired
	private WatchersServ watchers;
	
	@Autowired
	private ReportRepository reportRepository;
	
	
	@Autowired
	private ProtienPriceSynchJob job;
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void WatcherTest() throws Exception{
		watchers.protienWatcher();
	}
	
	@Test
	public void saveReportTest() throws Exception{
		Report r = new Report();
		r.setPrice(23.00);
		r.setProductName("Double Rich Chocolate");
		reportRepository.save(r);
	}
	
	@Test
	public void getLatestReport() throws Exception{
		List<Report> listOfReports =  reportRepository.findFirstByOrderByIdDesc();
		if(listOfReports!=null && listOfReports.size()>0) {
			System.out.println(listOfReports.get(0).getPrice());
		}else {
			System.out.println("Blank!!");
		}
	}
	
	@Autowired
	AlertRepository alertRepo;
	
	@Test
	public void alertRepoTest() {
		List<Alert> output = alertRepo.findByStatus(JobStatus.READY.toString());
		System.out.println("SizeOfTheAlert "+ output.size());
		
	}
	
	
	@Test
	public void saveSyncJob(){
		job.process();
	}

}

