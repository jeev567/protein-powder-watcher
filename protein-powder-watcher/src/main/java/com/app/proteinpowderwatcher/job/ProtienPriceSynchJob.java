package com.app.proteinpowderwatcher.job;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.app.proteinpowderwatcher.model.Alert;
import com.app.proteinpowderwatcher.model.Job;
import com.app.proteinpowderwatcher.model.Report;
import com.app.proteinpowderwatcher.repository.AlertRepository;
import com.app.proteinpowderwatcher.repository.JobRepository;
import com.app.proteinpowderwatcher.repository.ReportRepository;
import com.app.proteinpowderwatcher.service.WatchersServ;
import com.app.proteinpowderwatcher.util.JobStatus;

@Component
public class ProtienPriceSynchJob {

	@Autowired
	private WatchersServ watchers;

	@Value("${author}")
	private String author;
	
	@Value("${TWILIO.FROM}")
	private String fromNumber;
	
	@Value("${TWILIO.TO}")
	private String toNumber;

	@Autowired
	private ReportRepository reportRepository;
	
	@Autowired
	private JobRepository jobRepository;
	
	@Autowired
	private AlertRepository alertRepository;

	// TODO: Should read value from property file
	@Value("${cron.schedule.ProtienPriceSynchJob}")
	private String schedule;

	// Scheduled for every 1 min
	@Scheduled(cron = "0 0/1 * 1/1 * ?")
	public void process() {
		try {
			setJobStatus(JobStatus.IN_PROGRESS.toString());
			double newPrice = 0;
			// 0th:Flavour
			// 1st:Price
			//webscrap and brings the price of the Double Rich Chocoloate
			List<String> result = watchers.protienWatcher();
			List<Report> currentTopList = reportRepository.findFirstByOrderByIdDesc();
			if (currentTopList != null && currentTopList.size() > 0 && currentTopList.get(0).getPrice() == Double.valueOf(result.get(1).substring(1))) {
				Report current = currentTopList.get(0);
				current.setVersion(current.getVersion() + 1);
				reportRepository.save(current);
			} else {
				newPrice =  Double.valueOf(result.get(1).substring(1));
				Report savedOut = reportRepository.save(getObj(result));
				if (savedOut != null) {
					System.out.println("Report Successfully Saved");
					alertRepository.save(getAlertObj(savedOut,currentTopList!=null&& currentTopList.size()>0?currentTopList.get(0).getPrice():0,newPrice));
					System.out.println("Alert Successfully Saved");
				}
			}
			setJobStatus(JobStatus.COMPLETED.toString());

		} catch (Exception e) {
			System.out.println("Failed to save the report");
			setJobStatus(JobStatus.FAILED.toString());
		}
	}

	private Alert getAlertObj(Report savedOut, double oldPrice, double newPrice) {
		Alert a = new Alert();
		a.setAlertSentForId(String.valueOf(savedOut.getId()));
		a.setCreatedAt(new Date());
		a.setCreatedBy(author);
		a.setMessageSent("There is change in price for Protien Double Rich Chocolate from "+oldPrice +" to "+ newPrice );
		a.setSentFrom(fromNumber);
		a.setStatus(JobStatus.READY.toString());
		a.setSentTo(toNumber);
		a.setOldPrice(oldPrice);
		a.setNewPrice(newPrice);
		return a;
	}

	private Report getObj(List<String> result) {
		Report r = new Report();
		r.setPrice(Double.valueOf(result.get(1).substring(1)));
		r.setProductName(result.get(0));
		r.setCreatedBy(author);
		return r;
	}

	private void setJobStatus(String status) {
		Job job = new Job(this.getClass().getName().substring(33), status);
		job.setCreatedBy(author);
		jobRepository.save(job);

	}

}
