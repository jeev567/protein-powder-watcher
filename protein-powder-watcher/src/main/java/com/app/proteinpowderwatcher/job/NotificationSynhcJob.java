package com.app.proteinpowderwatcher.job;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.app.proteinpowderwatcher.model.Alert;
import com.app.proteinpowderwatcher.model.Job;
import com.app.proteinpowderwatcher.repository.AlertRepository;
import com.app.proteinpowderwatcher.repository.JobRepository;
import com.app.proteinpowderwatcher.service.NotificationServ;
import com.app.proteinpowderwatcher.util.JobStatus;

@Component
public class NotificationSynhcJob {
	
	@Value("${author}")
	private String author;
	
	@Autowired
	private JobRepository jobRepository;
	
	@Autowired
	private AlertRepository alertRepository;
	
	@Autowired
	private NotificationServ notificationServ;

	// TODO: Should read value from property file
	@Value("${cron.schedule.NotificationSynhcJob}")
	private String schedule;

	// Scheduled for every 1 min
	@Scheduled(cron = "0 0/1 * 1/1 * ?")
	public void process() {
		try {
			setJobStatus(JobStatus.IN_PROGRESS.toString());
			List<Alert> unporcesssed = alertRepository.findByStatus(JobStatus.READY.toString());
			for (Alert unprocessedAlert : unporcesssed) {
				try {
					unprocessedAlert.setStatus(JobStatus.IN_PROGRESS.toString());
					notificationServ.sendNotificationSms(unprocessedAlert);
					unprocessedAlert.setStatus(JobStatus.COMPLETED.toString());
				} catch (Exception e) {
					unprocessedAlert.setStatus(JobStatus.FAILED.toString());
				}
				alertRepository.save(unprocessedAlert);
			}
			setJobStatus(JobStatus.COMPLETED.toString());
		} catch (Exception e) {
			System.out.println("Failed to sSend notification the report");
			setJobStatus(JobStatus.FAILED.toString());
		}
	}

	private void setJobStatus(String status) {
		Job job = new Job(this.getClass().getName().substring(33), status);
		job.setCreatedBy(author);
		jobRepository.save(job);

	}

}
