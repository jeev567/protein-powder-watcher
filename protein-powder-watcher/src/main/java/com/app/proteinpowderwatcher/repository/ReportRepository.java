package com.app.proteinpowderwatcher.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.app.proteinpowderwatcher.model.Report;

public interface ReportRepository extends CrudRepository<Report, Integer> {

	public List<Report> findFirstByOrderByIdDesc();

}
