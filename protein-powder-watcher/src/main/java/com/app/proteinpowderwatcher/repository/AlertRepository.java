package com.app.proteinpowderwatcher.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.app.proteinpowderwatcher.model.Alert;

public interface AlertRepository  extends CrudRepository<Alert,Integer>{
		//TODO: Need to fetch in bactch wise mode
		public List<Alert> findByStatus(String status);
}
