package com.app.proteinpowderwatcher.repository;

import org.springframework.data.repository.CrudRepository;

import com.app.proteinpowderwatcher.model.Job;

public interface JobRepository  extends CrudRepository<Job,Integer>{

}
