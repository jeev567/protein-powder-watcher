package com.app.proteinpowderwatcher.util;

public enum JobStatus {
	COMPLETED, IN_PROGRESS, FAILED, READY
}
