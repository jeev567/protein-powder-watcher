package com.app.proteinpowderwatcher.exception;

public class NotificationException  extends RuntimeException{

	    /**
	 * 
	 */
	private static final long serialVersionUID = -7455598278672420883L;

		public NotificationException(String message) {
	        super(message);
	    }

	}

