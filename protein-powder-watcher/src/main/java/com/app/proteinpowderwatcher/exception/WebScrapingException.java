package com.app.proteinpowderwatcher.exception;

public class WebScrapingException  extends RuntimeException{

	    /**
	 * 
	 */
	private static final long serialVersionUID = -7455598278672420883L;

		public WebScrapingException(String message) {
	        super(message);
	    }

	}

