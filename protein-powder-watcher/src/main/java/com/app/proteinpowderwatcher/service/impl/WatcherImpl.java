package com.app.proteinpowderwatcher.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.app.proteinpowderwatcher.exception.WebScrapingException;
import com.app.proteinpowderwatcher.service.WatchersServ;
@Component
public class WatcherImpl implements WatchersServ{
	
	@Value( "${protien.url}" )
	private  String _url;
	
	@Value( "${protien.webdriver}" )
	private  String webdriver;
	
	@Value( "${protien.chrompath}" )
	private  String chromeDriverPath;
	
	@Value( "${protien.tag.flavor}" )
	private  String flavor;
	
	@Value( "${protien.tag.flavor.name}" )
	private  String flavorName;
	
	@Value( "${protien.tag.flavor.price}" )
	private  String falvorPrice;
	
	@Value( "${protien.tag.flavor.priceClass}" )
	private  String falvorPriceClass;

	public List<String> protienWatcher() throws WebScrapingException{
		WebDriver driver = null;
		List<String> output =  new ArrayList< String>();
		try {
			
			System.setProperty(webdriver,chromeDriverPath);
			/*ChromeOptions options = new ChromeOptions();  
			options.addArguments("--headless");*/ 
			ChromeOptions c = new ChromeOptions();
			c.setHeadless(true);
			driver = new ChromeDriver(c);
			driver.manage().window().maximize();
			driver.get(_url);
			WebElement k = driver.findElement(By.id(flavor));
			Thread.sleep(3000);
			String flavor = k.findElement(By.className(flavorName)).getText();
			Thread.sleep(3000);
			String price = k.findElement(By.id(falvorPrice)).findElement(By.className(falvorPriceClass)).getText();
			/*System.out.print(k.findElement(By.className(flavorName)).getText());
			System.out.print(" : ");
			System.out.print(k.findElement(By.id(falvorPrice)).findElement(By.className(falvorPriceClass)).getText());
			*/
			output.add( flavor);
			output.add( price);
			
		} catch (Exception e) {
			throw new WebScrapingException("Error occured while scraping Amazon: "+e);
		}finally{
			if(driver!=null)driver.close();
		}
		return output;
			}



}
