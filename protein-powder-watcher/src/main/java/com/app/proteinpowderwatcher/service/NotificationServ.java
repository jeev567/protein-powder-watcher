package com.app.proteinpowderwatcher.service;

import java.util.List;

import com.app.proteinpowderwatcher.exception.NotificationException;
import com.app.proteinpowderwatcher.model.Alert;

public interface NotificationServ {
	public void sendNotificationSms(Alert a) throws NotificationException;

}
