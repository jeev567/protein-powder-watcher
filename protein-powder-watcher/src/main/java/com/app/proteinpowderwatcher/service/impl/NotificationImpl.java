package com.app.proteinpowderwatcher.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.app.proteinpowderwatcher.exception.NotificationException;
import com.app.proteinpowderwatcher.model.Alert;
import com.app.proteinpowderwatcher.service.NotificationServ;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Component
public class NotificationImpl implements NotificationServ {
	
	
	@Value("${ACCOUNT_SID}")
	private String sid;

	@Value("${AUTH_TOKEN}")
	private String token;

	@Override
	public void sendNotificationSms(Alert a) throws NotificationException {
		
		if(a==null)return;
		try {
			Twilio.init(sid, token);
			Message message = Message.creator(new PhoneNumber(a.getSentTo()), new PhoneNumber(a.getSentFrom()),a.getMessageSent()).create();
			a.setTwilioMesssageStatus(message.getStatus().toString());
		} catch (Exception e) {
			e.printStackTrace();
			a.setTwilioMesssageStatus("Error at app level");
			throw new NotificationException(e.getMessage());
			
		}
	
	}

}
