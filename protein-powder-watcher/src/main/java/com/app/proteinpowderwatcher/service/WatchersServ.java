package com.app.proteinpowderwatcher.service;
import java.util.List;

import com.app.proteinpowderwatcher.exception.WebScrapingException;



public interface WatchersServ {
	List<String> protienWatcher() throws WebScrapingException;
}