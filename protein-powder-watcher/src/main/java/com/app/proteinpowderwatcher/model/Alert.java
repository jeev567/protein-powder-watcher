package com.app.proteinpowderwatcher.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Alert extends Base {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String sentTo;
	private String sentFrom;
	private String messageSent;
	private String status;
	private String alertSentForId;
	private double oldPrice;
	private double newPrice;
	private String twilioMesssageStatus;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSentTo() {
		return sentTo;
	}
	public void setSentTo(String sentTo) {
		this.sentTo = sentTo;
	}
	public String getSentFrom() {
		return sentFrom;
	}
	public void setSentFrom(String sentFrom) {
		this.sentFrom = sentFrom;
	}
	public String getMessageSent() {
		return messageSent;
	}
	public void setMessageSent(String messageSent) {
		this.messageSent = messageSent;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getAlertSentForId() {
		return alertSentForId;
	}
	public void setAlertSentForId(String alertSentForId) {
		this.alertSentForId = alertSentForId;
	}
	public double getOldPrice() {
		return oldPrice;
	}
	public void setOldPrice(double oldPrice) {
		this.oldPrice = oldPrice;
	}
	public double getNewPrice() {
		return newPrice;
	}
	public void setNewPrice(double newPrice) {
		this.newPrice = newPrice;
	}
	public String getTwilioMesssageStatus() {
		return twilioMesssageStatus;
	}
	public void setTwilioMesssageStatus(String twilioMesssageStatus) {
		this.twilioMesssageStatus = twilioMesssageStatus;
	}
	
	
	
}
