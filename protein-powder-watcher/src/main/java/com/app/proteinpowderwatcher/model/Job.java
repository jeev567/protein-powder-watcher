package com.app.proteinpowderwatcher.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.app.proteinpowderwatcher.util.JobStatus;

@Entity
public class Job extends Base {
	@Id
	@Column(length=50)
	private String jobName;
	public Job() {
		super();
	}
	private String Status;
	private String jobRunOn = super.getServerName();
	private long jobDurationMs;

	public Job(String name, String status) {
		this.jobName = name;
		this.Status = status;
		if(!JobStatus.IN_PROGRESS.toString().equals(status)) {
			jobDurationMs =  new Date().getTime() - super.getCreatedAt().getTime();
		}
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getJobRunOn() {
		return jobRunOn;
	}

	public void setJobRunOn(String jobRunOn) {
		this.jobRunOn = jobRunOn;
	}
}
